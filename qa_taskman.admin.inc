<?php

/**
 * @file qa_taskman.admin.inc
 *   Contains the admin page definition for the automated task infrastructure.
 */

function qa_taskman_admin_settings_form($form, &$form_state) {
  $form = array();

  $form['qasettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automated task infrastructure configuration settings'),
  );
  $form['qasettings']['dispatcher'] = array(
    '#type' => 'textfield',
    '#title' => t('Task Dispatcher URL'),
    '#description' => t('The URL of the task dispatcher server.'),
    '#default_value' => variable_get('QA_DISPATCHER_URL', 'conduit.example.com'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
