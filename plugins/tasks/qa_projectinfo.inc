<?php

/**
 * Drupal.org Project Info QA plugin
 */

// Standard ctools plugin declaration.
$plugin = array(
  'title' => t('Project Information'),
  'name' => 'qa_projectinfo',
  'is_default' => TRUE,
  'results' => 'qa_projectinfo_results',
  'prefix' => 'qa_projectinfo_prefix',
);

/**
 * Returns the introductory text for display in the results fieldset
 */
function qa_projectinfo_prefix() {
  $items = array(
    array('#markup' => '<h3>' . t('Project Information') . '</h3>'),
    array('#markup' => t('The table below contains basic information regarding this project and it\'s repositories.')),
    array('#markup' => '</br></br>'),
  );
  return drupal_render($items);
}

/**
 * Returns a render array for inclusion within the form used to render the
 * 'results' fieldset for this plugin on a project's 'Quality Assurance'
 * results tab.
 *
 * @param object $node
 *   The project's $node object
 * @param array $results
 *   Contents of the qa_taskman_results table for this project/plugin
 *   combination.  Should be an empty array(), as this plugin does not store
 *   any results in the database.
 */
function qa_projectinfo_results($node, $results) {
  $pid = $node->nid;
  $plugin = 'qa_projectinfo';

  // Retrieve verious project info items out of the node object items for
  // inclusion in the results.
  $project_type = $node->field_project_type[LANGUAGE_NONE][0]['value'];
  $machine_name = $node->field_project_machine_name[LANGUAGE_NONE][0]['value'];
  $repo_type = $node->versioncontrol_project['repo']->vcs;
  $owner = user_load($node->uid);

  // Generate the repository URI.  This code is drupal.org specific.
  if ($project_type == 'sandbox') {
    $repo = QA_TASKMAN_REPO_PREFIX . "/sandbox/{$owner->git_username}/{$node->nid}.{$repo_type}";
  }
  else {
    $repo = QA_TASKMAN_REPO_PREFIX . "/project/{$machine_name}.{$repo_type}";
  }

  // Construct the 'Project information' section render array
  // I Title                      I Shortname
  // I Project Type               I Nid
  // I Has Issue Queue            I Has Releases
  // I Repository URL             I Default Branch

  $project_info = array(
    '#theme' => 'table',
    '#header' => array(),
    '#rows' => array(
      array(
        array('data' => '<b>' . t('Title:') . ' </b>' . $node->title . '</br>'),
        array('data' => '<b>' . t('Shortname:') . ' </b>' . $node->field_project_machine_name[LANGUAGE_NONE][0]['value'] . '</br>'),
      ),
      array(
        array('data' => '<b>' . t('Project Type:') . ' </b>' . $node->field_project_type[LANGUAGE_NONE][0]['value'] . '</br>'),
        array('data' => '<b>' . t('Nid:') . ' </b>' . $node->nid . '</br>'),
      ),
      array(
        array('data' => '<b>' . t('Has Issue Queue:') . ' </b>' . ($node->field_project_has_issue_queue[LANGUAGE_NONE][0]['value'] ? 'Yes' : 'No' ) . '</br>'),
        array('data' => '<b>' . t('Has Releases:') . ' </b>' . ($node->field_project_has_releases[LANGUAGE_NONE][0]['value'] ? 'Yes' : 'No' ) . '</br>'),
      ),
      array(
        array('data' => '<b>' . t('Repository URL:') . ' </b>' . $repo . '</br>'),
        array('data' => '<b>' . t('Default Branch:') . ' </b>' . $node->versioncontrol_project['repo']->default_branch . '</br>'),
      ),
    ),
    '#attributes' => array(
      'class' => 'project_issue',
    ),
  );

  // Construct the the final render array for passing back to the
  // qa_taskman_results form.
  $items['githealth-results'] = array(
    'project-info' => array(
      '#type' => 'container',
      'children' => $project_info
    ),
  );

  return $items;
}

