<?php

/**
 * Drupal.org Simpletest Testing QA plugin
 */

// Standard ctools plugin declaration.
$plugin = array(
  'title' => t('Simpletest Testing'),
  'name' => 'qa_simpletest',
  'settings' => 'qa_simpletest_settings',
  'prefix' => 'qa_simpletest_prefix',
  'results' => 'qa_simpletest_results',
);

/**
 * Returns the introductory text for display in the results fieldset
 */
function qa_simpletest_prefix() {
  $items = array(
    array('#markup' => '<h3>' . t('Simpletest Tests') . '</h3>'),
    array('#markup' => t("This tab contains the Simpletest test results for this project's branches/tags.")),
    array('#markup' => '</br></br>'),
  );
  return drupal_render($items);
}

/**
 * Returns form elements for rendering within the 'settings' fieldset for this
 * plugin on a project's 'Quality Assurance' project tab.
 *
 * @param $node
 *   The project's $node object
 */
function qa_simpletest_settings($form, &$form_state, $node) {
  // Grab relevant information from the $node object.
  $pid = $node->nid;
  $plugin = 'qa_simpletest';
  $repo = $node->versioncontrol_project['repo_id'];

  // Generate branch/tag arrays for this repository
  $labels = qa_taskman_get_project_labels_from_repo_id($repo);
  $branches = array();
  foreach ($labels['branches'] as $label) {
    $branches[$label->label_id] = $label->name;
  }
  $tags = array();
  foreach ($labels['tags'] as $label) {
    $tags[$label->label_id] = $label->name;
  }

  // Retrieve existing settings from the {qa_taskman_settings} table
  $settings = qa_taskman_project_all_plugin_settings($pid, $plugin);

  $form['pid'] = array(
    '#type' => 'value',
    '#value' => $pid,
  );

  $form['qa_simpletest'] = array(
    '#type' => 'fieldset',
    '#title' => 'Simpletest Testing',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['qa_simpletest']['#states'] = array(
    'visible' => array(
      ':input[name="pluginlist[' . $plugin . ']"]' => array('checked' => TRUE),
    ),
  );

  // Introductory text
  $form['qa_simpletest']['intro_text'] = array(
    array('#markup' => t('Select the branches for which you would like to enable automatic simpletest execution after each branch commit, ')),
    array('#markup' => t('or for each patch uploaded to the project issue queue.')),
    array('#markup' => '</br></br>'),
  );

  // Create a select box listing other branches which may be added to the table
  // The "#options' parameter will be added later, once we have parsed the list
  // of enabled branches out of the plugin settings.
  $form['qa_simpletest']['add'] = array(
    '#type' => 'select',
    '#title' => t('Select from the list below to enable simpletest testing for additional branch/tags:'),
  );

  // Table of enabled branches/tags
  $form['qa_simpletest']['table_intro'] = array(
    array('#markup' => t('This plugin has been enabled for the following branches:')),
    array('#markup' => '</br></br>'),
  );

  // Table header
  $form['qa_simpletest']['branchtable']['#header'] = array(
    'branch' => t('Branch/Tag'),
    'branchtests' => t('Branch Testing'),
    'patchtests' => t('Patch Testing'),
  );

  // Initialize variables to hold original form values
  $enabled_branches = array();
  $enabled_patches = array();

  // Generate the per-branch form rows
  foreach ($settings as $setting) {
    // Look for branch-specific settings
    if ($setting->status && $setting->vid != 0) {
      // Generate the branch/tag label
      $vcs_label = isset($branches[$setting->vid]) ? $branches[$setting->vid] : $tags[$setting->vid];
      // Populate branch plugin status entries
      $options[$setting->vid] = '';
      $status[0][$setting->vid] = $setting->vid;
      $enabled_branches[$setting->vid] = $vcs_label;
      // Retrieve 'patch tests' settings
      $data = unserialize($setting->settings);
      if ($data['patchtests']) {
        $status[1][$setting->vid] = $setting->vid;
        $enabled_patches[] = $setting->vid;
      }
      // Generate form element
      $form['qa_simpletest']['branchtable']['rows'][$setting->vid] = array(
        'branch' => array(
          '#type' => 'item',
          '#markup' => $vcs_label,
        ),
      );
    }
  }

  // Ensure we have at least one branch enabled
  if (count($enabled_branches) != 0) {
    // Generate 'Branch Testing' form checkboxes
    $form['qa_simpletest']['branchtable']['checkboxes'][1] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => isset($status[0]) ? $status[0] : array(),
      '#attributes' => array('class' => array()),
    );
    // Generate 'Patch Testing' form checkboxes
    $form['qa_simpletest']['branchtable']['checkboxes'][2] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => isset($status[1]) ? $status[1] : array(),
      '#attributes' => array('class' => array()),
    );
    // Set a theme function, so that the form is rendered as a table
    $form['qa_simpletest']['branchtable']['#theme'] = 'qa_taskman_checkbox_table';
  }
  else {
    // No branches enabled.  Suppress the table output.
    unset($form['qa_simpletest']['branchtable']);
    $form['qa_simpletest']['table_intro'] = array(
      array('#markup' => t('This plugin is not currently active on any branch. ')),
      array('#markup' => t('Select a branch from the list above to enable simpletest testing.')),
      array('#markup' => '</br></br>'),
    );
  }
  // Store the original values in the form for use in submit processing
  $form['all_branches'] = array(
    '#type' => 'value',
    '#value' => $branches,
  );
  $form['original_branches'] = array(
    '#type' => 'value',
    '#value' => $enabled_branches,
  );
  $form['original_patches'] = array(
    '#type' => 'value',
    '#value' => $enabled_patches,
  );

  // Update the 'Add new tags/branches' select box with an '#options' parameter
  $options = array('0' => '-- no new entries --') + array_diff_key($branches, $enabled_branches);
  $form['qa_simpletest']['add']['#options'] = $options;

  // Generate a form 'submit' button
  $form['qa_simpletest']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;

}

/**
 * Performs plugin-specific validation processing on the settings form.
 */
function qa_simpletest_settings_validate($form, &$form_state) {
  // Ensure at least one branch was originally active.
  if (count($form_state['values']['original_branches']) != 0) {
    // Patch tests may only be enabled if branch tests are enabled.
    foreach ($form_state['values']['qa_simpletest']['branchtable']['checkboxes'][1] as $key => $item) {
      if ($key != $item) {
        if ($form_state['values']['qa_simpletest']['branchtable']['checkboxes'][2][$key] == $key) {
          form_set_error('branchtable', t('Branch testing must be enabled to enable patch testing.'));
        }
      }
    }
  }
}

/**
 * Performs plugin-specific submit processing on the settings form.
 */
function qa_simpletest_settings_submit($form, &$form_state) {
  $pid = $form_state['values']['pid'];
  // Ensure at least one branch was originally active.
  if (count($form_state['values']['original_branches']) != 0) {
    // Process branch testing checkboxes
    foreach ($form_state['values']['qa_simpletest']['branchtable']['checkboxes'][1] as $key => $item) {
      if ($key != $item) {
        // Branch testing disabled
        if (in_array($key, array_keys($form_state['values']['original_branches']))) {
          qa_simpletest_set_branchtests($key, $pid, FALSE);
          drupal_set_message(t("Simpletest testing disabled for @branch", array('@branch' => $form_state['values']['original_branches'][$key])));
        }
      }
      else {
        // Branch testing enabled.  Check patch testing status
        if ($form_state['values']['qa_simpletest']['branchtable']['checkboxes'][2][$key] == $key && !in_array($key, $form_state['values']['original_patches'])) {
          // Patch testing enabled
          qa_simpletest_set_patchtests($key, $pid, TRUE);
          drupal_set_message(t("Simpletest patch testing enabled for @branch", array('@branch' => $form_state['values']['original_branches'][$key])));
        }
        elseif ($form_state['values']['qa_simpletest']['branchtable']['checkboxes'][2][$key] != $key && in_array($key, $form_state['values']['original_patches'])) {
          // Patch testing disabled
          qa_simpletest_set_patchtests($key, $pid, FALSE);
          drupal_set_message(t("Simpletest patch testing disabled for @branch", array('@branch' => $form_state['values']['original_branches'][$key])));
        }
      }
    }
  }
  // Check if any new branches/tags have been added.
  if ($form_state['values']['qa_simpletest']['add'] != 0) {
    $label_id = $form_state['values']['qa_simpletest']['add'];
    qa_simpletest_set_branchtests($label_id, $pid, TRUE);
    drupal_set_message(t("Simpletest patch testing enabled for @branch", array('@branch' => $form_state['values']['all_branches'][$label_id])));
  }
}

/**
 * Enable/Disable simpletest branch testing for a given branch
 */
function qa_simpletest_set_branchtests($branch_id, $pid = 0, $enabled = TRUE) {
  // Do not attempt to enter a record with an empty project id
  if (empty($pid)) {
    return;
  }
  $status = $enabled ? 1 : 0;
  $exists = db_query("SELECT 1 FROM {qa_taskman_settings} WHERE plugin = 'qa_simpletest' AND pid = :pid AND vid = :vid", array(':pid' => $pid, ':vid' => $branch_id))->fetchField();
  if ($exists) {
    $query = db_update('qa_taskman_settings')
      ->fields(array('status' => $status))
      ->condition('plugin', 'qa_simpletest')
      ->condition('vid', $branch_id)
      ->execute();
  }
  else {
    $query = db_insert('qa_taskman_settings')
      ->fields(array(
        'pid' => $pid,
        'plugin' => 'qa_simpletest',
        'vid' => $branch_id,
        'status' => $status,
        'settings' => serialize(array('patchtests' => FALSE)),
      ))
      ->execute();
  }
}

/**
 * Enable/Disable simpletest patch testing for a given branch
 */
function qa_simpletest_set_patchtests($branch_id, $pid = 0, $enabled = TRUE) {
  // Do not attempt to enter a record with an empty project id
  if (empty($pid)) {
    return;
  }
  $settings = serialize(array('patchtests' => $enabled));
  $query = db_update('qa_taskman_settings')
    ->fields(array('settings' => $settings))
    ->condition('plugin', 'qa_simpletest')
    ->condition('pid', $pid)
    ->condition('vid', $branch_id)
    ->execute();
}


/**
 * Returns a render array for inclusion within the form used to render the
 * 'results' fieldset for this plugin on a project's 'Quality Assurance'
 * results tab.
 *
 * @param $node
 *   The projects $node object
 * @param $results
 *
 */
function qa_simpletest_results($node, $results) {
  $pid = $node->nid;
  $plugin = 'qa_simpletest';

  // If no results found for this project/plugin combination, return a message
  // stating so.
  if (empty($results)) {
    return array(
      '#markup' => t('There are no results available to display for this plugin.'),
    );
  }

  // Store various node object items which will be included in our 'Simpletest
  // testing' section of the results.
  $project_type = $node->field_project_type[LANGUAGE_NONE][0]['value'];
  $machine_name = $node->field_project_machine_name[LANGUAGE_NONE][0]['value'];

  // Construct the the final render array for passing back to the
  // qa_taskman_results form.

  return $items;
}

