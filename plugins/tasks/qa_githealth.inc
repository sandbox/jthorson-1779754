<?php

/**
 * Drupal.org Git Health Check QA plugin
 */

// Standard ctools plugin declaration.
$plugin = array(
  'title' => t('Git Repo Health Check'),
  'name' => 'qa_githealth',
  'settings' => 'qa_githealth_settings',
  'results' => 'qa_githealth_results',
  'summary' => 'qa_githealth_summary',
  'prefix' => 'qa_githealth_prefix',
);

/**
 * Returns the settings fieldset for the qa_githealth plugin
 */
function qa_githealth_settings() {
  $form['qa_githealth'] = array(
    '#type' => 'fieldset',
    '#title' => 'Git Repository Health Check',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['qa_githealth']['#states'] = array(
    'visible' => array(
      ':input[name="pluginlist[qa_githealth]"]' => array('checked' => TRUE),
    ),
  );
  $form['qa_githealth']['text'] = array(
    '#markup' => t('There are no configurable settings for this plugin.'),
  );
  return $form;
}


/**
 * Returns the introductory text for display in the results fieldset
 */
function qa_githealth_prefix() {
  $items = array(
    array('#markup' => '<h3>' . t('Git Repository Health Check') . '</h3>'),
    array('#markup' => t('This plugin audits the git repository for this project, checking for a number of common configuration issues or errors.')),
    array('#markup' => '</br></br>'),
  );
  return drupal_render($items);
}

/**
 * Returns a render array for inclusion within the form used to render the
 * 'results' fieldset for this plugin on a project's 'Quality Assurance'
 * results tab.
 *
 * @param $node
 *   The project's $node object
 */
function qa_githealth_results($node, $results) {
  $pid = $node->nid;
  $plugin = 'qa_githealth';

  // If no results found for this project/plugin combination, return a message
  // stating so.
  if (empty($results)) {
    return array(
      '#markup' => t('There are no results available to display for this plugin.'),
    );
  }

  // Construct the table headers
  $branchheader = array('Label', 'Repository Links');
  $tagheader = array('Label', 'Has Release', 'Repository Links');
  $issueheader = array('Status', 'Message', 'Item', 'Reference');

  // Datafill the table information
  $branches = array();
  $tags = array();
  $issues = array();

  // Retrieve the branch, tag, and issue table data.
  // We store 'extra' data which doesn't map directly into the
  // {qa_taskman_results} database table into the serialized 'data' column.
  // Most plugins probably would not store this much information locally (as
  // they would simply store summary information per branch), but adding this
  // information here demonstrates the ability for a plugin to do more if
  // needed.
  if (!empty($results->data)) {
    $data = unserialize($results->data);
    // TODO:  This assumes that the data is stored in the perfect format for
    // outputting as $rows in theme_table; which probably isn't true.
    $branches = $data['branches'];
    $tags = $data['tags'];
    $issues = $data['issues'];
  }

  // Construct the the final render array for passing back to the
  // qa_taskman_results form.
  $items['githealth-results'] = array();
  if (!empty($branches)) {
    $items['githealth-results']['branches'] = array(
      '#theme' => 'table',
      '#header' => $branchheader,
      '#rows' => $branches,
    );
  }
  else {
    $items['githealth-results']['branches'] = array(
      '#markup' => t('No branches found.') . '</br>',
    );
  }
  if (!empty($tags)) {
    $items['githealth-results']['tags'] = array(
      '#theme' => 'table',
      '#header' => $tagheader,
      '#rows' => $tags,
    );
  }
  else {
    $items['githealth-results']['tags'] = array(
      '#markup' => t('No tags found.') . '</br>',
    );
  }
  if (!empty($issues)) {
    $items['githealth-results']['issues'] = array(
      '#theme' => 'table',
      '#header' => $issueheader,
      '#rows' => $issues,
    );
  }
  else {
    $items['githealth-results']['issues'] = array(
      '#markup' => t('No issues found.') . '</br>',
    );
  }

  return $items;
}

/**
 * Generates a summary string for the vertical tab
 */
function qa_githealth_summary($node, $results) {
  if (empty($results[0]->brief)) {
    return t('No results available');
  }
  else {
    return check_plain($results[0]->brief);
  }
}
