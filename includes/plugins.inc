<?php
/**
 * @file
 * Provide ctools plugin definitions and implementation hooks.
 *
 * @see http://panels3.dev.logrus.com/help/ctools/plugins-creating
 * @see http://panels3.dev.logrus.com/help/ctools/plugins-implementing
 */

/**
 * Fetch metadata on a specific plugin.
 *
 * @param $type
 *   Plugin type.
 * @param $plugin
 *   Name of plugin.
 * @return
 *   An array of information about the requested plugin:
 *   @code
 *     'plugin1' => array(
 *       'name' => 'machine_name',
 *       'title' => 'human_name',
 *       'description' => t('description of this plugin'),
 *       'settings' => 'plugin_settings_form_definition_function',
 *       'results' => 'plugin_results_form_definition_function',
 *     )
 *   @endcode
 */
function qa_taskman_plugins_get($type, $plugin) {
  ctools_include('plugins');
  return ctools_get_plugins('qa_taskman', $type, $plugin);
}

/**
 * Fetch metadata for all plugins.
 *
 * @return
 *   An array of arrays with information about all available plugins:
 *   @code
 *     array(
 *       'plugin1' => array(
 *         'name' => 'machine_name',
 *         'title' => 'human_name',
 *         'description' => t('description of this plugin'),
 *         'settings' => 'plugin_settings_form_definition_function',
 *         'results' => 'plugin_results_form_definition_function',
 *       )
 *     )
 *   @endcode
 */
function qa_taskman_plugins_get_all($type) {
  ctools_include('plugins');
  return ctools_get_plugins('qa_taskman', $type);
}

/**
 * Fetch metadata for all default plugins.
 *
 * @return
 *   An array of arrays with information about all default plugins:
 *   @code
 *     array(
 *       'plugin1' => array(
 *         'name' => 'machine_name',
 *         'title' => 'human_name',
 *         'description' => t('description of this plugin'),
 *         'settings' => 'plugin_settings_form_definition_function',
 *         'results' => 'plugin_results_form_definition_function',
 *       )
 *     )
 *   @endcode
 */
function qa_taskman_plugins_get_default($type) {
  $plugins = qa_taskman_plugins_get_all($type);
  foreach ($plugins as $plugin) {
    if (empty($plugin['is_default'])) {
      unset($plugins[$plugin['name']]);
    }
  }
  return $plugins;
}

/**
 * Fetch metadata for all non-default plugins.
 *
 * @return
 *   An array of arrays with information about all non-default plugins:
 *   @code
 *     array(
 *       'plugin1' => array(
 *         'name' => 'machine_name',
 *         'title' => 'human_name',
 *         'description' => t('description of this plugin'),
 *         'settings' => 'plugin_settings_form_definition_function',
 *         'results' => 'plugin_results_form_definition_function',
 *       )
 *     )
 *   @endcode
 */
function qa_taskman_plugins_get_non_default($type) {
  $plugins = qa_taskman_plugins_get_all($type);
  foreach ($plugins as $plugin) {
    if (!empty($plugin['is_default'])) {
      unset($plugins[$plugin['name']]);
    }
  }
  return $plugins;
}

/**
 * Implements hook_ctools_plugin_type().
 */
function qa_taskman_ctools_plugin_type() {
  return array(
    'tasks' => array(),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function qa_taskman_ctools_plugin_directory($module, $plugin) {
  if ($module == 'qa_taskman' && $plugin == 'tasks') {
    return 'plugins/' . $plugin;
  }
}

