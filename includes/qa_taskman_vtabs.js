(function ($) {

/**
 * Custom summary for the qa_taskman vertical tabs.
 */
Drupal.behaviors.qa_taskman = {
  attach: function (context, settings) {
    $.each(settings.qa_taskman.summaries, function(key, object) {
      $('fieldset.qa-taskman-plugin-'.concat(object.plugin), context).drupalSetSummary(function (context) {
    	  return object.summary;
      }
    });
  }
};

})(jQuery);
