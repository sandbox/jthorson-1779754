<?php

/**
 * @file qa_taskman.pages.inc
 *   Contains page layout forms for automated task infrastructure project pages
 */

/**
 * Generates the 'Settings' page on the 'Quality Assurance' tab of a project.
 */
function qa_taskman_project_settings_page($node) {
  $form = drupal_get_form('qa_taskman_project_settings_form', $node);
  $output = drupal_render($form);

  // Retrieve the lists of plugins on this site, for rendering the individual
  // task plugin settings forms.
  $defaultplugins = qa_taskman_plugins_get_default('tasks');
  $nondefaultplugins = qa_taskman_plugins_get_non_default('tasks');
  $plugins = qa_taskman_plugins_get_all('tasks');

  // Generate the individual task plugin settings forms.
  // The actual content of the forms is defined in the plugin itself.
  foreach ($plugins as $plugin) {
    // Does the plugin have a 'settings' function defined?
    if (!empty($plugin['settings'])) {
      // If so, add the form to our output.
      $form = drupal_get_form($plugin['settings'], $node);
      $output .= drupal_render($form);
    }
  }

  return $output;
}


/**
 * Generates the 'Settings' form on the 'Quality Assurance' tab of a project.
 */
function qa_taskman_project_settings_form($form, &$form_state, $node) {
  $form = array();

  // Retrieve the lists of plugins on this site, for rendering the plugin list.
  $defaultplugins = qa_taskman_plugins_get_default('tasks');
  $nondefaultplugins = qa_taskman_plugins_get_non_default('tasks');
  $plugins = qa_taskman_plugins_get_all('tasks');

  // If we have no plugins, return a status message instead of an empty list.
  if (!$plugins) {
    $form['markup'] = array(
      '#markup' => t('There are currently no automated task plugins installed on this site.'),
    );
    return $form;
  }

  // Store the project node id in the form, so it can be retrieved during
  // processing.
  $form['project']['pid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );

  // 'Available Plugins' list container
  $form['plugins'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Quality Assurance plugins'),
    '#weight' => -1,
    '#collapsible' => FALSE,
  );

  // Generate the 'default plugins' listing
  foreach ($defaultplugins as $plugin) {
    $options[$plugin['name']] = $plugin['title'];
  }
  $form['plugins']['defaultlist'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#value' => $options,
    '#disabled' => TRUE,
    '#title' => t('Default plugins (always enabled):'),
  );

  // Get the list of currently enabled non-default plugins for this project.
  $enabled = qa_taskman_project_enabled_plugins($node->nid);

  // Generate the 'available plugins' listing
  $options = array();
  foreach ($nondefaultplugins as $plugin) {
    $options[$plugin['name']] = $plugin['title'];
  }
  $form['plugins']['pluginlist'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $enabled,
    '#title' => t('Use the checkboxes below to indicate which optional plugins you wish to enable for this project:'),
  );

  // Store the 'enabled' plugin values so that we can reference them later.
  $form['project']['enabled'] = array(
    '#type' => 'value',
    '#value' => $enabled,
  );

  // Generate the 'Save' button for the list.
  $form['plugins']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
/*
  // Generate the individual task plugin settings fields
  // The actual content of the settings fields is defined in the plugin itself.
  // TODO: Currently, processing is performed by adding to the '#submit' array
  // on the form, but it would be better for each plugin to define it's own
  // 'submit' button which directs to a dedicate processing array so that
  // changes made in plugin settings fieldsets are not unintentionally updated
  // when a user enables or disables a plugin via the list above.
  foreach ($plugins as $plugin) {
    // Does the plugin have a 'settings' function defined?
    if (!empty($plugin['settings'])) {
      // Create the plugin settings container
      $form[$plugin['name']] = array(
        '#type' => 'fieldset',
        '#title' => $plugin['title'],
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      // If this is a non-default plugin, we only display the settings if the
      // associated plugin checkbox is checked.
      if (empty($plugin['is_default'])) {
        $form[$plugin['name']]['#states'] = array(
          'visible' => array(
            ':input[name="pluginlist[' . $plugin['name'] . ']"]' => array('checked' => TRUE),
          ),
        );
      }
      // We get the settings pane contnet from the function defined in the
      // $plugin['settings'] parameter, and pass it the project node object.
      $form[$plugin['name']][$plugin['name'] . '_settings'] = $plugin['settings']($node);

      // We need to explicitly declare the default validate/submit routines so
      // they do not get clobbered when adding plugin-specific functions to the
      // arrays.  See #1284642.
      $form['#validate'][] = 'qa_taskman_project_settings_form_validate';
      $form['#submit'][] = 'qa_taskman_project_settings_form_submit';
      if (isset($plugin['validate'])) {
        $form['#validate'][] = $plugin['validate'];
      }
      if (isset($plugin['submit'])) {
        $form['#submit'][] = $plugin['submit'];
      }
    }
  }
  */
  return $form;
}

/**
 * Validate the 'Available Plugins' settings fieldset
 */
function qa_taskman_project_settings_form_validate($form, &$form_state) {
  $node = node_load($form_state['values']['pid']);
  if (!project_node_is_project($node)) {
    form_set_error('', t('Error: Invalid project detected in form submission.'));
  }
}

/**
 * Process the 'Available Plugins List' settings fieldset
 */
function qa_taskman_project_settings_form_submit($form, &$form_state) {
  foreach ($form_state['values']['pluginlist'] as $plugin => $enabled) {
    // Update any newly enabled plugins
    if ($enabled && !in_array($plugin, $form_state['values']['enabled'])) {
      qa_taskman_project_enable_plugin($form_state['values']['pid'], $plugin);
    }
    // Update any newly disabled plugins
    if (!$enabled && in_array($plugin, $form_state['values']['enabled'])) {
      qa_taskman_project_disable_plugin($form_state['values']['pid'], $plugin);
    }
  }
}

/**
 * Generates the 'Results' form on the 'Quality Assurance' tab of a project.
 */
function qa_taskman_project_results_form($form, &$form_state, $node) {
  $form = array();

  // Retrieve the list of enabled plugins for this project
  $enabled = qa_taskman_project_enabled_plugins($node->nid);

  // Retrieve the list of default plugins
  $defaultplugins = qa_taskman_plugins_get_default('tasks');

  // Place default plugins at the top of the stack
  $plugins = array();
  foreach ($defaultplugins as $plugin) {
    $plugins[$plugin['name']] = $plugin['name'];
  }

  // Add the enabled plugins to the stack after the default plugins.
  if (!empty($enabled)) {
    $plugins += $enabled;
  }

  // If we have no plugins, return a status message.
  if (empty($plugins)) {
    $form['markup'] = array(
      '#markup' => t('There are currently no automated task plugins enabled for this project.'),
    );
    return $form;
  }

  // Create the vertical tabs container which will store our results for each
  // plugin.
  $form['plugin_tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  // Initialize a variable used for passing vertical tab summaries to js.
  $summaries = array();

  // Assemble the plugin results array to be displayed as vertical tabs
  foreach ($plugins as $item) {
    // Load the plugin item
    $plugin = qa_taskman_plugins_get('tasks', $item);

    // Create the vertical tab for this plugin.
    $form[$plugin['name']] = array(
      '#type' => 'fieldset',
      '#title' => $plugin['title'],
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'plugin_tabs',
      '#attributes' => array('class' => array('qa-taskman-plugin', 'qa-taskman-plugin-' . $plugin['name'])),
      '#attached' => array(
        'js' => array(
          'vertical_tabs' => drupal_get_path('module', 'qa_taskman') . '/includes/qa_taskman_vtabs.js',
        ),
      ),
    );

    if (isset($plugin['prefix'])) {
      // Generate pre-results information if desired
      $form[$plugin['name']]['prefix'] = array(
        '#markup' => $plugin['prefix'](),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
    }

    // Retrieve any available plugin results from the database
    $results = qa_taskman_project_plugin_results($node->nid, $plugin['name']);

    // Output the results elements as defined by the $plugin['result'] function.
    $form[$plugin['name']][$plugin['name'] . '_results'] = $plugin['results']($node, $results);

    // Populate the array of plugins and summaries to be passed to javascript.
    if (!empty($plugin['summary'])) {
      $summaries[] = array(
        'plugin' => $plugin['name'],
        'summary' => $plugin['summary']($node, $results),
      );
    }
  }

  // Send summaries to js so that they can be added to the vertical tabs.
  drupal_add_js(array('qa_taskman' => array('summaries' => $summaries)), 'setting');

  return $form;
}
