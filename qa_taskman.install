<?php

/*
 * @file
 *   Install file for the automation infrastructure "Task Manager" module.
 */
/**
 * Implements hook_schema().
 *
 * Note: Must enable on project with vid=0 before enabling individual branches.
 */
function qa_taskman_schema() {
  $schema['qa_taskman_settings'] = array(
    'description' => 'Stores qa task manager plugin settings for each project.',
    'fields' => array(
      'pid' => array(                   // Project node ID
        'description' => 'Project ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'plugin' => array(                // Plugin machine name
        'description' => 'QA Plugin machine name',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'vid' => array(                   // VCS Label ID
        'description' => 'Project VCS label ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(                // 0 => disabled, 1 => enabled
        'description' => 'Plugin status (enabled/disabled).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'settings' => array(
        'description' => 'Additional settings for this project/plugin combo.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
    ),
    'indexes' => array(
      'pid' => array('pid'),
      'plugin' => array('plugin'),
      'vid' => array('vid'),
    ),
    'primary key' => array(
      'pid', 'plugin', 'vid'
    ),
  );
  $schema['qa_taskman_results'] = array(
    'description' => 'Stores per-project qa task manager plugin results.',
    'fields' => array(
      'pid' => array(                   // Project node ID
        'description' => 'Project ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'plugin' => array(                // Plugin machine name
        'description' => 'QA Plugin machine name',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'vid' => array(                   // VCS Label ID
        'description' => 'Project VCS label ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'brief' => array(
        'description' => 'Brief result summary',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'result' => array(                // Intended for Pass/Fail status, etc.
        'description' => 'Generic Result field',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(                // Requested, queued, processing, awaiting response, postponed, result, custom, etc.
        'description' => 'Test status (active, pending, postponed, etc.)',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'resource' => array(              // related qa.d.o resource
        'description' => 'qa.drupal.org resource id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'results' => array(
        'description' => 'Summary results for this plugin/project/branch combo.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
      ),
      'data' => array(
        'description' => 'Additional data for this plugin/project/branch combo.',
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),

    ),
    'indexes' => array(
      'pid' => array('pid'),
      'plugin' => array('plugin'),
      'vid' => array('vid'),
    ),
    'primary key' => array(
      'pid', 'plugin', 'vid'
    ),
  );
  return $schema;
}
